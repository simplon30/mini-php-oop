<?php

use App\Salle;

require '../vendor/autoload.php';

//Salle 1
$cgr = new Salle();
$cgr->setName("Salle Ice");
$cgr->setCapacity(4);

//Ajoutes des Places
$cgr->AddPlace(1, 2, 15);

$cgr->AddPlace(2, 2, 15);

$cgr->AddPlace(3, 2, 15);

$cgr->AddPlace(4, 2, 15);

//C'est complet
$cgr->AddPlace(5, 2, 15);

//reserve place 1
$cgr->getPlace(0)->addReservation("21-01-2011", "21:00", "Jean");
$cgr->getPlace(0)->addReservation("21-01-2011", "22:30", "Jean");

//place déjà prise
$cgr->getPlace(0)->addReservation("21-01-2011", "21:00", "Jon");

// //reserve place 2
$cgr->getPlace(1)->addReservation("21-01-2011", "21:00", "June");

// //reserve place 3
$cgr->getPlace(2)->addReservation("21-01-2011", "00:00", "Jean");

// //reserve place 4
$cgr->getPlace(3)->addReservation("21-01-2011", "21:00", "Plouf");

// //reserve place 5 COMPLET
//$cgr->getPlace(4)->addReservation("21-01-2011", "21:00", "Pototao");


var_dump($cgr);
var_dump($cgr->getPlace(1));

//valeur Null car impossible de prendre une place
var_dump($cgr->getPlace(4));

//Est ce que la salle est full ?
var_dump($cgr->isFull());

//Calcul des benefices total
$benefices = $cgr->beneficies();
echo "Total des bénéfices de la salle : " . $benefices . "<br>";

echo "<br>";

//Obtiens toutes les reservation de ($nom du client)
$reservations = $cgr->getAllReservation("Jean");
foreach ($reservations as $reservation) {
    echo "Réservation pour le créneau horaire: " . $reservation->getDate() . " " . $reservation->getHour() . " Effectuée par : " . $reservation->getName() . "<br>";
}









