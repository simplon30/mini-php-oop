<?php

namespace App;

class Reservation {
    private string $date;

    private string $hour;

    private string $name;


    /**
     * @param string $date date e la séance
     * @param string $hour heure de la séance
     * @param string $name nom du client
     */
    public function __construct(string $date, string $hour, string $name) {
    	$this->date = $date;
    	$this->hour = $hour;
        $this->name = $name;
    }

	/**
	 * @return string
	 */
	public function getDate(): string {
		return $this->date;
	}
	
	/**
	 * @return string
	 */
	public function getHour(): string {
		return $this->hour;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
}