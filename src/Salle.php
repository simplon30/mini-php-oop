<?php

namespace App;

class Salle
{
    private string $name;

    private int $capacity;

    private array $place = [];

    /**
     * Obtiens toutes les reservation d'un ($clientName)
     * @param mixed $clientName Entrer le nom du client à chercher
     * @return array
     */
    public function getAllReservation($clientName){

        $reservation = [];

        foreach ($this->place as $place) {

            foreach ($place->getReservation() as $reservation) {
                if($reservation->getName() == $clientName) {
                    $reservations[] = $reservation;
                }
            }
        }
        return $reservations;
    }


    /**
     * Calcul les bénéfices pour toutes les place prise possible
     * @return float|int
     */
    public function beneficies(){
        $beneficie = 0;
        foreach($this->place as $place) {
            
                $beneficie += $place->getPrice();
            
        }
        return $beneficie;
    }

    /**
     * 
     * @param int $number Numéro de la Place
     * @param int $location Localisation de le Place
     * @param int $price Prix de la Place
     * @return void
     */
    public function AddPlace(int $number, int $location, int $price){

        foreach ($this->place as $place) {
            
            if ($place->getNumber() == $this->capacity) {

                echo "La Salle est complète, sorry !<br>";
                return;

            }
        }
        
        $this->place[] = new Place($number, $location, $price);

        echo "La Place est prise avec succes ! <br>";

    }

    /**
     * Calcul le nombre de place et la comparre à la capacité de la salle, si la salle est full return string "est full"
     * @return void
     */
    public function isFull(){
        foreach ($this->place as $place) {
            
            if ($place->getNumber() == $this->capacity) {

                echo "Oui, la salle est complete ! <br>";

            }
        }
    }

    public function getPlace(int $index): ?Place
    {
        if (isset($this->place[$index])) {
            return $this->place[$index];
        }
        return null;
    }

	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getCapacity(): int {
		return $this->capacity;
	}
	
	/**
	 * @param int $capacity 
	 * @return self
	 */
	public function setCapacity(int $capacity): self {
		$this->capacity = $capacity;
		return $this;
	}

	
	/**
	 * @param array $place 
	 * @return self
	 */
	public function setPlace(array $place): self {
		$this->place = $place;
		return $this;
	}
}