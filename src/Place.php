<?php

namespace App;

class Place
{
    private int $number;

    private int $location;

    private int $price;

    private array $reservation = [];




    public function addReservation(string $date, string $hour, string $name){

        foreach ($this->reservation as $reservation) {
            if($reservation->getDate() == $date && $reservation->getHour() == $hour ) {
                // echo "Déjà pris <br>";
                return;
            }
        }
        $this->reservation[] = new Reservation($date, $hour, $name);
        //echo "place reservé <br>";

    }

    /**
     * 
     * @param int $number Numéro de la Place
     * @param int $location Localisation de la Place
     * @param int $price Prix de la Place
     */
    public function __construct(int $number, int $location, int $price)
    {
        $this->number = $number;
        $this->location = $location;
        $this->price = $price;

    }


	/**
	 * @return int
	 */
	public function getNumber(): int {
		return $this->number;
	}
	
	/**
	 * @return int
	 */
	public function getLocation(): int {
		return $this->location;
	}
	
	/**
	 * @return int
	 */
	public function getPrice(): int {
		return $this->price;
	}

	/**
	 * @return array
	 */
	public function getReservation(): array {
		return $this->reservation;
	}
}